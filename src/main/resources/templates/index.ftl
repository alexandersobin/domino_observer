<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Главная страница</title>
    <link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body scroll = "no">
<h1>ЭЦП Мониторинг</h1>
<table class="container">
    <thead>
    <tr>
        <th><h1>${tableHeader["serverTitle"]}</h1></th>
        <th><h1>${tableHeader["aliasTitle"]}</h1></th>
        <th><h1>${tableHeader["databaseTitle"]}</h1></th>
        <th><h1>${tableHeader["agentTitle"]}</h1></th>
        <th><h1>${tableHeader["lastRunTitle"]}</h1></th>
        <th><h1>${tableHeader["timeDiffTitle"]}</h1></th>
        <th><h1>${tableHeader["alertTitle"]}</h1></th>
    </tr>
    </thead>
    <tbody>
        <#list tableData as tableDataEntry>
            <tr>
                <td>${tableDataEntry["server"]}</td>
                <td>${tableDataEntry["alias"]}</td>
                <td>${tableDataEntry["database"]}</td>
                <td>${tableDataEntry["agent"]}</td>
                <td>${tableDataEntry["lastRun"]}</td>
                <td>${tableDataEntry["timeDiff"]}</td>
                <td>${tableDataEntry["alert"]}</td>
            </tr>
        </#list>
    </tbody>
</table>
</body>
</html>