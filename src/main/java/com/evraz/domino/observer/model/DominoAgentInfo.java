package com.evraz.domino.observer.model;

import lotus.domino.NotesException;
import lotus.domino.NotesFactory;
import lotus.domino.NotesThread;
import lotus.domino.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DominoAgentInfo implements IModel<String, String > {

    private Session session;
    private List<Map<String ,String>> outputData;
    private Map<String ,String> outputMetaData;

    public DominoAgentInfo() throws NotesException {
        NotesThread.sinitThread();
        session = NotesFactory.createSession();
        outputData = new ArrayList<>();
        outputMetaData = new HashMap<>();
    }

    @Override
    public List<Map<String, String>> getData() throws NotesException {
        if (outputMetaData.isEmpty())
            getMetaData();

        for (int i = 0; i < 10; i++) {
            Map<String, String> data = new HashMap<>();
            data.put("server", "mockServer");
            data.put("alias", "mockAlias");
            data.put("database", "mockDatabase");
            data.put("agent", "mockAgent");
            data.put("lastRun", "mockLastRun");
            data.put("timeDiff", "mockTimeDiff");
            data.put("alert", "mockAlert");
            outputData.add(data);
        }

        return outputData;
    }

    @Override
    public Map<String, String> getMetaData() {
        outputMetaData.put("serverTitle", "Сервер");
        outputMetaData.put("aliasTitle", "Алиас");
        outputMetaData.put("databaseTitle", "База");
        outputMetaData.put("agentTitle", "Агент");
        outputMetaData.put("lastRunTitle", "Последний запуск");
        outputMetaData.put("timeDiffTitle", "Временной интервал");
        outputMetaData.put("alertTitle", "Тревога");

        return outputMetaData;
    }
}
