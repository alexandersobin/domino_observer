package com.evraz.domino.observer.model;

import lotus.domino.NotesException;

import java.util.List;
import java.util.Map;

public interface IModel<K, V> {

    List<Map<K, V>> getData() throws NotesException;
    Map<K, V> getMetaData();
}
