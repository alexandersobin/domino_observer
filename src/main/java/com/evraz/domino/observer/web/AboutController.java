package com.evraz.domino.observer.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.concurrent.ThreadLocalRandom;

@Controller
public class AboutController {

    @RequestMapping({"/sobin", "/about"})
    public String about(Model model) {
        model.addAttribute("days", ThreadLocalRandom.current().nextInt(1, 100));
        return "about";
    }
}
