package com.evraz.domino.observer.web;

import com.evraz.domino.observer.model.DominoAgentInfo;
import lotus.domino.NotesException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping("/")
    public String index(Model model) throws NotesException {
        DominoAgentInfo domioAgentInfo = new DominoAgentInfo();
        model.addAttribute("tableHeader",domioAgentInfo.getMetaData());
        model.addAttribute("tableData", domioAgentInfo.getData());
        return "index";
    }
}
